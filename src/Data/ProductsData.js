

export const productsData =
{
    products: [
        {
            id: 1,
            title: "Red Shirt",
            type: "Clothes",
            price: "300",
            image: "redShirt.jpg",

        },
        {
            id: 2,
            title: "Blue Shirt",
            type: "Clothes",
            price: "350",
            image: "blueShirt.jpg",

        },
        {
            id: 3,
            title: "Pink Shirt",
            type: "Clothes",
            price: "400",
            image: "pinkShirt.jpg",

        },
        {
            id: 4,
            title: "White Shirt",
            type: "Clothes",
            price: "360",
            image: "whiteShirt.jpg",

        },
        {
            id: 5,
            title: "Violet Shirt",
            type: "Clothes",
            price: "340",
            image: "violetShirt.jpg",

        },
        {
            id: 6,
            title: "Black Pant",
            type: "Clothes",
            price: "700",
            image: "blackPant.jpg",

        },
        {
            id: 7,
            title: "Blue Pant",
            type: "Clothes",
            price: "800",
            image: "bluePant.jpg",

        },
        {
            id: 8,
            title: "Green TShirt",
            type: "Clothes",
            price: "400",
            image: "greenTshirt.jpg",

        },
        {
            id: 9,
            title: "Gray TShirt",
            type: "Clothes",
            price: "450",
            image: "grayTshirt.jpg",

        },
        {
            id: 10,
            title: "Blue Trackpant",
            type: "Clothes",
            price: "700",
            image: "blueTrackpant.jpg",

        },
    ]
}


export default productsData;