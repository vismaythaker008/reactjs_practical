import { Provider } from 'react-redux';
import { Redirect, Route, BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import CartPage from './components/Cart/CartPage';
import Navbar from './components/Navbar/Navbar';
import OrderSuccessPage from './components/OrderSuccess/OrderSuccessPage';
import ProductListingPage from './components/Product-Listing/ProductListingPage';
import store from './store';

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Navbar />
        <Route exact path="/">
          <Redirect to="/Product-Listing"></Redirect>
        </Route>
        <Route path="/Product-Listing" exact component={ProductListingPage} />
        <Route path="/Cart" exact component={CartPage} />
        <Route path="/Order-Successful" exact component={OrderSuccessPage} />
      </Provider>
    </Router>
  );
}

export default App;
