import React from 'react'
import { useSelector } from 'react-redux';
import '../../css/Product-Listing/ProductListingPage.css';
import SingleProduct from './SingleProduct';

const ProductListingPage = () => {
    const allProducts = useSelector(state => state.ProductsReducer);
    return (
        <div className="containerProductListing">
            <div className="singleProductRow">
                {allProducts.products.map(product => (
                    <SingleProduct product={product} key={product.id} />
                ))}
            </div>
        </div >
    )
}

export default ProductListingPage
