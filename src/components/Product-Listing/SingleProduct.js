import React from 'react'
import "../../css/Product-Listing/SingleProduct.css";
import { Link } from 'react-router-dom';
import { Col, Button } from 'reactstrap';
import { useDispatch } from 'react-redux';

const SingleProduct = ({ product }) => {
    const dispatch = useDispatch();
    return (
        <Col sm={{ span: 12 }} md={{ span: 8 }} className="product-border" >
            <div className="product">
                <div className="product-img">
                    <img src={`/product-images/${product.image}`} alt={product.title} />
                </div>
                <div className="product-name">{product.title}</div>
                <div className="row">
                    <div className="col-6">
                        <div className="product-price">
                            Rs.{new Intl.NumberFormat('en-US').format(product.price)}
                        </div>
                        <div className="add-to-cart">
                            <Link to="/cart">
                                <Button color="success" onClick={() => dispatch({ type: 'ADD_TO_CART', payload: { product: product } })} > Add to Cart</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </Col>
    )
}

export default SingleProduct
