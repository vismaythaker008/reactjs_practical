import { Button } from 'reactstrap'
import React from 'react'
import { Link } from 'react-router-dom'
import "../../css/Order-Success/OrderSuccess.css"

const OrderSuccessPage = () => {
    return (
        <div className="orderSuccessMessage">
            <div className="orderSuccessMessage">
                Your Order has been Placed
            </div>
            <Link to="/Product-Listing">
                <Button className="goToHomeBtn" >Return to Shopping</Button>
            </Link>
        </div>
    )
}

export default OrderSuccessPage
