import React from 'react'
import "../../css/Navbar/Navbar.css";
import { Link } from 'react-router-dom'
import { BsFillBucketFill } from "react-icons/bs";
import { useSelector } from 'react-redux';

const Navbar = () => {
    const { totalUniqueItems } = useSelector(state => state.CartReducer)
    return (
        <nav className="navbar navbar-inverse sticky-top">
            <div className="container-fluid">
                <div className="navbar-header">
                    <Link to="/">E-com</Link>
                </div>

                <ul className="nav navbar-nav navbar-right">
                    <Link to="/cart">
                        <div className="basket">
                            <BsFillBucketFill className="cart-icon" />
                            <span>{totalUniqueItems}</span>
                        </div>
                    </Link>
                </ul>
            </div>
        </nav>
    )
}
export default Navbar
