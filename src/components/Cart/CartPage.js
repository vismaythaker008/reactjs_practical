import { useSelector } from 'react-redux';
import '../../css/Cart/CartPage.css'
import CartEmpty from './CartEmpty';
import CartSingleItem from './CartSingleItem'
import CartSummary from './CartSummary'


const CartPage = () => {
    const cartState = useSelector(state => state.CartReducer);
    return (
        <div className="cart"  >
            <h3>Cart Details</h3>
            {cartState.products.length > 0 ? <>
                <div className="cartInfo flex-direction">
                    <div className="col-9 ">
                        <div className="table-responsive">
                            <table className="table ">
                                <thead>
                                    <tr className="cart-heading">
                                        <th>Image</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                        <th>Remove</th>
                                    </tr>
                                </thead>
                                <tbody className="cart-body">
                                    {cartState.products.map(product => (
                                        <CartSingleItem product={product} key={product.id} />
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <CartSummary cartState={cartState} />
                </div>
            </> : <CartEmpty />}

        </div >
    )
}

export default CartPage
