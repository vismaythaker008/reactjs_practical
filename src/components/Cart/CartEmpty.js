import { Button } from 'reactstrap'
import React from 'react'
import { Link } from 'react-router-dom'
import '../../css/Cart/CartEmpty.css'
const CartEmpty = () => {

    return (
        <div >

            <div className="cart-empty-message">
                Your Cart is Empty
        </div>
            <Link to="/">
                <Button className="return-to-shop">Start Shopping</Button>
            </Link>
        </div>

    )
}

export default CartEmpty
