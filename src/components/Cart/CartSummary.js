import React from 'react'
import '../../css/Cart/CartSummary.css'
import { Button } from 'reactstrap'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

const CartSummary = ({ cartState }) => {
    const dispatch = useDispatch();
    return (
        <div className="cart-summary">
            <div className="cart-summary-heading">
                Summary
            </div>
            <div className="cart-summary-details">
                <div className="row mb-10">
                    <div className="col-9 cart-detail">
                        Order Total :
                    </div>
                    <div className="col-3">
                        Rs.{new Intl.NumberFormat('en-US').format(cartState.totalPrice)}
                    </div>
                </div>
                <div className="row mb-10">
                    <div className="col-9 cart-detail">
                        Shipping Charges(LKR):
                    </div>
                    <div className="col-3">
                        Rs.{new Intl.NumberFormat('en-US').format(cartState.shippingCharges)}
                    </div>
                </div>
                <div className="row mb-10">
                    <div className="col-9 cart-detail">
                        Sub Total:
                    </div>
                    <div className="col-3">
                        Rs.{new Intl.NumberFormat('en-US').format(cartState.totalPrice + cartState.shippingCharges)}
                    </div>
                </div>
                <div className="row mb-10">
                    <div className="col-9 cart-detail">
                        Tax({cartState.tax}%):
                    </div>
                    <div className="col-3">
                        Rs.{new Intl.NumberFormat('en-US', { maximumFractionDigits: 2 }).format(cartState.taxPrice)}
                    </div>
                </div>
                <div className="row mb-10">
                    <div className="col-9 cart-detail">
                        Total Price:
                    </div>
                    <div className="col-3">
                        Rs.{new Intl.NumberFormat('en-US', { maximumFractionDigits: 2 }).format(cartState.finalPriceAfterTax)}
                    </div>
                </div>
                <Link to="/Order-Successful">
                    <Button className="checkoutBtn" onClick={() => dispatch({ type: "REMOVE_ALL", })} >Pay Now</Button>
                </Link>
                <Button className="removeAllBtn" onClick={() => dispatch({ type: "REMOVE_ALL", })}>Remove All</Button>

            </div>
        </div>
    )
}

export default CartSummary
