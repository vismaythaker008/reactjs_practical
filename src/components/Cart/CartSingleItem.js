import React from 'react';
import '../../css/Cart/CartSingleItem.css';
import { BsDash, BsPlus, BsReverseBackspaceReverse } from 'react-icons/bs';
import { useDispatch } from 'react-redux';

const CartSingleItem = ({ product }) => {
    const dispatch = useDispatch();
    return (
        <tr >
            <td >
                <div className="cart-image">
                    <img src={`/product-images/${product.image}`} alt={product.title} />
                </div>
            </td>
            <td>{product.title}</td>
            <td>Rs.{new Intl.NumberFormat('en-US').format(product.price)}</td>
            <td>
                <div className="details-info">
                    <div className="details-incDec ">
                        <span className="decrement" onClick={() => dispatch({ type: "DEC", payload: product.id })} ><BsDash /></span>
                        <span className="quantity">{product.quantity}</span>
                        <span className="increment" onClick={() => dispatch({ type: "INC", payload: product.id })} ><BsPlus /></span>
                    </div>
                </div>
            </td>
            <td>Rs.{new Intl.NumberFormat('en-US').format(product.price * product.quantity)}</td>
            <td>
                <div className="cart-remove" onClick={() => dispatch({ type: "REMOVE", payload: product.id })}>
                    <BsReverseBackspaceReverse />
                </div>
            </td>
        </tr>
    )
}

export default CartSingleItem
