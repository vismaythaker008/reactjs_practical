const initstate = {
    products: [],
    totalPrice: 0,
    finalPriceAfterTax: 0,
    //subTotal - total cart price +shipping charges
    subTotal: 0,
    //taxPrice - amount of tax on the subTotal
    taxPrice: 0,
    totalUniqueItems: 0,
    totalQuantities: 0,
    shippingCharges: 500,
    tax: 1.23
}
const CartReducer = (state = initstate, action) => {
    let selectedProduct = null;
    let index;
    let cartTotalPrice;
    let cartTotalQuantities;
    let cartTotalUniqueItems;
    let cartFinalPriceAfterTax, cartSubTotal, cartTaxPrice;
    const remove = () => {
        //fliters this product from the products array so we get the updated products array
        const finalProducts = state.products.filter(product => product.id !== action.payload)
        //as we are removing untique product we decrement the total unique items count
        cartTotalUniqueItems = state.totalUniqueItems - 1;
        cartTotalPrice = state.totalPrice - (selectedProduct.price * selectedProduct.quantity);
        cartTotalQuantities = state.totalQuantities - selectedProduct.quantity;
        CalculateFinalPriceData();
        return {
            ...state, products: finalProducts, totalPrice: cartTotalPrice, totalUniqueItems: cartTotalUniqueItems, totalQuantities: cartTotalQuantities,
            subTotal: cartSubTotal, taxPrice: cartTaxPrice, finalPriceAfterTax: cartFinalPriceAfterTax
        }


    }
    const removeAll = () => {

        const finalProducts = state.products.filter(product => product.id === -1)
        return {
            ...state, products: finalProducts, totalPrice: 0, totalUniqueItems: 0, totalQuantities: 0,
            subTotal: 0, taxPrice: 0, finalPriceAfterTax: 0
        }
    }
    const Increment = () => {
        cartTotalPrice = state.totalPrice + parseInt(selectedProduct.price);
        //finds the index at which that product is placed in the state array
        index = state.products.findIndex(product => product.id === selectedProduct.id);
        //increases the quantity by 1 of that product
        cartTotalQuantities = state.totalQuantities + 1;
        state.products[index].quantity += 1;
        CalculateFinalPriceData();
        return {
            ...state, totalPrice: cartTotalPrice, totalQuantities: cartTotalQuantities, finalPriceAfterTax: cartFinalPriceAfterTax,
            subTotal: cartSubTotal, taxPrice: cartTaxPrice

        }

    }
    const CalculateFinalPriceData = () => {
        cartSubTotal = cartTotalPrice + state.shippingCharges;
        cartTaxPrice = (state.tax * cartSubTotal / 100);
        cartFinalPriceAfterTax = cartSubTotal + cartTaxPrice;

    }
    switch (action.type) {
        case 'ADD_TO_CART':
            //This is called when add to cart is clicked
            selectedProduct = action.payload.product;

            //checks if the id of product to be added is present in the state's products array or not 
            //which means if it is already present in the cart or not
            const checkIfExists = state.products.find(product => product.id === selectedProduct.id);
            if (checkIfExists) {
                selectedProduct = checkIfExists;
                return Increment();
            }
            else {
                //set the initial qunatity to 1 
                selectedProduct.quantity = 1;
                cartTotalPrice = state.totalPrice + parseInt(selectedProduct.price);
                cartTotalQuantities = state.totalQuantities + 1;
                //as we have added new product we increment the total unique items count
                cartTotalUniqueItems = state.totalUniqueItems + 1;
                CalculateFinalPriceData();
                return {
                    ...state, products: [...state.products, selectedProduct], totalUniqueItems: cartTotalUniqueItems, totalPrice: cartTotalPrice, totalQuantities: cartTotalQuantities,
                    finalPriceAfterTax: cartFinalPriceAfterTax, subTotal: cartSubTotal, taxPrice: cartTaxPrice
                }
            }


        case 'REMOVE':
            //checks if this product is already in cart or not
            selectedProduct = state.products.find(product => product.id === action.payload);
            if (selectedProduct) {
                return remove();
            }
            break;
        case 'INC':
            selectedProduct = state.products.find(product => product.id === action.payload);
            if (selectedProduct) {
                return Increment();
            }
            else {
                return state;
            }

        case 'DEC':
            //checks if this product is already in cart or not
            selectedProduct = state.products.find(product => product.id === action.payload);
            if (selectedProduct && selectedProduct.quantity > 1) {
                index = state.products.findIndex(product => product.id === action.payload);
                //decreases the quantity by 1 of that product
                selectedProduct.quantity -= 1;
                // and here it is replaced with the product in the state array
                state.products[index] = selectedProduct;
                cartTotalPrice = state.totalPrice - parseInt(selectedProduct.price);
                cartTotalQuantities = state.totalQuantities - 1;
                CalculateFinalPriceData();
                return {
                    ...state, totalPrice: cartTotalPrice, totalQuantities: cartTotalQuantities,
                    finalPriceAfterTax: cartFinalPriceAfterTax, subTotal: cartSubTotal, taxPrice: cartTaxPrice
                }
            }
            else {
                // remove the product from array if the quantity is decremented from 1
                return state;
            }

        case 'REMOVE_ALL':
            return removeAll();
        default:
            return state;
    }
}

export default CartReducer
