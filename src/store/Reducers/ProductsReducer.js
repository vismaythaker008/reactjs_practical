//gets all product from the products json array
import initalState from "../../Data/ProductsData"

const ProductsReducer = (state = initalState, action) => {
    switch (action.type) {

        default:
            //returns the state containing all the products information
            return state;
    }
}

export default ProductsReducer
