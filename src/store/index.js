import { combineReducers, createStore } from "redux"
import ProductsReducer from "./Reducers/ProductsReducer"
import { devToolsEnhancer } from 'redux-devtools-extension';
import CartReducer from "./Reducers/CartReducer";
const root = combineReducers(
    {
        ProductsReducer,
        CartReducer
    }
);

//function to save cart data to local storage
function saveToLocalStorage(state) {
    try {
        const serialisedState = JSON.stringify(state);
        localStorage.setItem("persistantState", serialisedState);

    } catch (e) {
        console.warn(e);
    }
}

//function to load cart data from local storage
function loadFromLocalStorage() {
    try {
        const serialisedState = localStorage.getItem("persistantState");
        if (serialisedState === null) return undefined;
        return JSON.parse(serialisedState);
    } catch (e) {
        console.warn(e);
        return undefined;
    }
}
//dev tools are used for debugging of redux state datain the webpage
const store = createStore(root, loadFromLocalStorage(), devToolsEnhancer());
store.subscribe(() => saveToLocalStorage(store.getState()));
export default store;
